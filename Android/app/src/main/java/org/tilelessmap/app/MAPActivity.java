package org.tilelessmap.app;

import org.libsdl.app.SDLActivity;


import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.provider.Settings;
import android.util.Log;
import androidx.core.content.ContextCompat;
import androidx.core.app.ActivityCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreference;

import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import java.io.File;
import java.io.Serializable;


/*
 * A sample wrapper class that just calls SDLActivity
 */

public class MAPActivity extends SDLActivity
{

    private static final String TAG = "TilelessMap";
    private static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 20;
    public static String the_file;
    public static String the_dir;

    private static int the_val;
    private MAPActivity ma;
    private IdentifyData[] identifyData = null;

    public static final String EXTRA_IDENTIFY_DATA = "org.tilelessmap.app.IDENTIFY_DATA";
    @Override
    protected String[] getArguments() {
        String[] str = new String[4];
        str[0] = "-f";
        str[1] = MAPActivity.the_file;
        //       str[1] = "/storage/emulated/0/Download/sverige_sld.tileless";
        //       str[1] = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        str[2] = "-d";
        str[3] = MAPActivity.the_dir + "/";
        //     str[3] = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getParent();
        Log.i(TAG, "File to use is: " + str[1]);
        Log.i(TAG, "Dir to use is: " + str[3]);
        return str;
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        Intent intent = getIntent();
        ma = this;
        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = p.edit();
        editor.putBoolean("gps", false);
        editor.commit();

        the_file = p.getString("project_file","");
        the_dir = p.getString("project_directory","");
        Log.i(TAG, "the_file: " + the_file);
        Log.i(TAG, "the_dir: " + the_dir);
        super.onCreate(savedInstanceState);

        if ( ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
        {
            ActivityCompat.requestPermissions( this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION  },MY_PERMISSION_ACCESS_FINE_LOCATION );
        }

       View view = LayoutInflater.from(this).inflate(R.layout.main, null);
       addContentView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
              LinearLayout.LayoutParams.MATCH_PARENT));

        Button getLayerList = (Button) findViewById(R.id.get_layer_list);
        getLayerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MAPActivity.this, LayerList.class);
                startActivity(intent);
            }
        });

        final ToggleButton info = (ToggleButton) findViewById(R.id.select_geom);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button ib = (Button) findViewById(R.id.info_button);
                if(info.isChecked()) {
                    InfoMode(1, ma);
                    ib.setVisibility(View.VISIBLE);
                }
                else {
                    InfoMode(0, ma);
                    ib.setVisibility(View.INVISIBLE);
                }
            }
        });

        Button settings = (Button) findViewById(R.id.settings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MAPActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });

        Button info_button = (Button) findViewById(R.id.info_button);
        info_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(identifyData != null && identifyData.length > 0) {

                    Intent info_intent = new Intent(MAPActivity.this, IdentifyActivity.class);
                    info_intent.putExtra(EXTRA_IDENTIFY_DATA, (Serializable) identifyData);
                    startActivity(info_intent);
                }
            }
        });




    }


    public void identify_info(IdentifyData[] ld) {
        identifyData = ld;
        return;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults){
        switch (requestCode) {
            case MY_PERMISSION_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
            }
        }
    }


    protected void onDestroy() {

        Intent i = new Intent(MAPActivity.this, Gps.class);
        //i.putExtra("name", "SurvivingwithAndroid");
        MAPActivity.this.stopService(i);
    //    gps.stopUsingGPS();
        super.onDestroy();
    }


    private native int InfoMode(int start, MAPActivity ma);
}


