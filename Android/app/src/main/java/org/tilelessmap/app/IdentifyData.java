package org.tilelessmap.app;

import java.io.Serializable;

public class IdentifyData implements Serializable {
    public String layerName;
    public int id;
    public String colName;
    public String val;
}
