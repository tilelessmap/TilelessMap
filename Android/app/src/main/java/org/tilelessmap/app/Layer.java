package org.tilelessmap.app;

import java.io.Serializable;

public class Layer implements Serializable {
    public String name;
    public String groupName;
    public boolean visible;
    public boolean groupVisible;
    public boolean querable;
    int id;
    String db_alias;
}
