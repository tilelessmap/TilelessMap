package org.tilelessmap.app;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;


public class Gps extends Service implements LocationListener{

    private boolean running = false;
    double latitude; // latitude
    double longitude; // longitude
    double acc; // accuracy
    public LocationManager locationManager;
    Intent intent;

    private int mintime;
    private int mindist;
    public static native void onNativeGPS(double latitude, double longitude, double acc);

    @Override
    public void onCreate() {
        super.onCreate();
        System.loadLibrary("main");
        intent = new Intent("tileless_gps");
    }

    @Override
    public void onDestroy() {
            running = false;
            // handler.removeCallbacks(sendUpdatesToUI);
            super.onDestroy();
            Log.v("STOP_SERVICE", "DONE");
            locationManager.removeUpdates(this);
            onNativeGPS(1000, 1000, 0);
        super.onDestroy();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mintime = intent.getIntExtra("mintime", mintime);
        mindist = intent.getIntExtra("mindist", mindist);

        //the_file = intent.getStringExtra(HomeActivity.EXTRA_THE_FILE);
        //the_dir = intent.getStringExtra(HomeActivity.EXTRA_THE_DIR);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return START_NOT_STICKY;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, mintime * 1000, mindist, this);
        running = true;
        return START_NOT_STICKY;
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public boolean isStarted() {
        return running;
    }


    public void onLocationChanged(Location location)
    {

        if ( Build.VERSION.SDK_INT >= 23)
        {

            int permission_test = ActivityCompat.checkSelfPermission( MAPActivity.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION );
            if(permission_test!= PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        Log.d("GPS","a change is here");
        if (locationManager != null) {
            location = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                acc = location.getAccuracy();
                onNativeGPS(latitude, longitude, acc);
                Log.d("GPS", "lat = " + latitude + "lon = " + longitude);
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderDisabled(String provider)
    {
        Toast.makeText( getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT ).show();
    }


    public void onProviderEnabled(String provider)
    {
        Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
    }

}
