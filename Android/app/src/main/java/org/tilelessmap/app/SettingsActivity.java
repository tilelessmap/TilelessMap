package org.tilelessmap.app;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;

public class SettingsActivity extends AppCompatActivity implements
        PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    private static final String TITLE_TAG = "settingsActivityTitle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new HeaderFragment())
                    .commit();
        } else {
            setTitle(savedInstanceState.getCharSequence(TITLE_TAG));
        }
        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            setTitle(R.string.title_activity_settings);
                        }
                    }
                });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save current activity title so we can set it again after a configuration change
        outState.putCharSequence(TITLE_TAG, getTitle());
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getSupportFragmentManager().popBackStackImmediate()) {
            return true;
        }
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragmentCompat caller, Preference pref) {
        // Instantiate the new Fragment
        final Bundle args = pref.getExtras();
        final Fragment fragment = getSupportFragmentManager().getFragmentFactory().instantiate(
                getClassLoader(),
                pref.getFragment(),
                args);
        fragment.setArguments(args);
        fragment.setTargetFragment(caller, 0);
        // Replace the existing Fragment with the new Fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.settings, fragment)
                .addToBackStack(null)
                .commit();
        setTitle(pref.getTitle());
        return true;
    }

    public static class HeaderFragment extends PreferenceFragmentCompat {

        Gps gps_service;
        private boolean isBound = false;

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

            setPreferencesFromResource(R.xml.header_preferences, rootKey);

            SwitchPreferenceCompat gps_sw = (SwitchPreferenceCompat) findPreference("gps");

            SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getActivity());
            gps_sw.setChecked(p.getBoolean("gps", false));
            gps_sw.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                     @Override
                                                     public boolean onPreferenceChange(Preference preference, Object newValue) {

                                                         Intent i = new Intent(getActivity(), Gps.class);
                                                         if((Boolean) newValue == true) {
                                                             SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                                             int min_time_default = Integer.parseInt(requireActivity().getResources().getString(R.string.gps_min_time_default));
                                                             int mintime = p.getInt("gps_mintime", min_time_default);
                                                             int min_dist_default = Integer.parseInt(requireActivity().getResources().getString(R.string.gps_min_dist_default));
                                                             int mindist = p.getInt("gps_mindist", min_dist_default);


                                                             i.putExtra("mintime", mintime);
                                                             i.putExtra("mindist", mindist);
                                                             getActivity().startService(i);
                                                         }
                                                         else
                                                             getActivity().stopService(i);

                                                         return true;
                                                     }
                                                 }
            );

            ListPreference identify_tolerance_pref = (ListPreference) findPreference("identify_tolerance");

            identify_tolerance_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    int identity_tol = Integer.parseInt(newValue.toString());
                    IdentifyTolerance(identity_tol);
                    return true;
                }
            });



        }

    }

    public static class GPSFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

            setPreferencesFromResource(R.xml.gps_preferences, rootKey);



            ListPreference gps_time = (ListPreference) findPreference("gps_time");
            gps_time.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                       @Override
                                                       public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                           //i.putExtra("name", "SurvivingwithAndroid");
                                                           String gps_time_val_str = newValue.toString();
                                                           int gps_time_val = Integer.parseInt(gps_time_val_str);

                                                           SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                                           SharedPreferences.Editor editor = p.edit();
                                                           editor.putInt("gps_mintime", gps_time_val);
                                                           editor.commit();
                                                           int mindist = p.getInt("gps_mindist", 10);

                                                           Intent i = new Intent(getActivity(), Gps.class);

                                                           getActivity().stopService(i);

                                                           i.putExtra("mintime", gps_time_val);
                                                           i.putExtra("mindist", mindist);
                                                           getActivity().startService(i);

                                                           return true;
                                                       }
                                                   }
            );

            ListPreference gps_dist = (ListPreference) findPreference("gps_dist");
            gps_dist.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                                       @Override
                                                       public boolean onPreferenceChange(Preference preference, Object newValue) {
                                                           //i.putExtra("name", "SurvivingwithAndroid");
                                                           String gps_dist_val_str = newValue.toString();
                                                           int gps_dist_val = Integer.parseInt(gps_dist_val_str);

                                                           SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getActivity());
                                                           SharedPreferences.Editor editor = p.edit();
                                                           editor.putInt("gps_mindist", gps_dist_val);
                                                           editor.commit();
                                                           int mintime = p.getInt("gps_mintime", 10);

                                                           Intent i = new Intent(getActivity(), Gps.class);

                                                           getActivity().stopService(i);

                                                           i.putExtra("mindist", gps_dist_val);

                                                           i.putExtra("mintime", mintime);
                                                           i.putExtra("mindist", gps_dist_val);
                                                           getActivity().startService(i);
                                                           return true;
                                                       }
                                                   }
            );

        }
    }

    private static native int IdentifyTolerance(int tol);
}


