package org.tilelessmap.app;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


class CustomIdentifyAdapter extends BaseExpandableListAdapter {


    private Context context;
    private LinkedHashMap<String, List<IdentifyData>> IdentifyDetails;
    List<String> IdentifyHeaders;


    public CustomIdentifyAdapter(Context context, List<String> IdentifyHeaders, LinkedHashMap<String, List<IdentifyData>> IdentifyDetails) {

        this.context = context;
        this.IdentifyDetails = IdentifyDetails;
        this.IdentifyHeaders = IdentifyHeaders;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.IdentifyHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.IdentifyHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);


        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.identify_header, null);        }


         TextView tv = convertView.findViewById(R.id.layer_name_id);
        tv.setText(listTitle);

        return convertView;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.IdentifyDetails.get(this.IdentifyHeaders.get(groupPosition)).get(childPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.IdentifyDetails.get(this.IdentifyHeaders.get(groupPosition)).size();
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition,final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final IdentifyData identifyData = (IdentifyData) getChild(groupPosition, childPosition);

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.identify_details, null);        }


        TextView colName = (TextView) convertView.findViewById(R.id.identify_column_name);
        colName.setText(identifyData.colName);

        TextView colVal = (TextView) convertView.findViewById(R.id.identify_val);
        colVal.setText(identifyData.val);


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

}

class ExpandableIdentifyData {
    public static LinkedHashMap<String, List<IdentifyData>> getData(IdentifyData[] details_list)
    {
        LinkedHashMap<String, List<IdentifyData>> expandableListDetail = new LinkedHashMap<String, List<IdentifyData>>();

        for (IdentifyData detail:details_list)
        {
            String key_ = detail.layerName + " - " + detail.id;
            if(expandableListDetail.containsKey(key_) == false) {
                List<IdentifyData> l = new ArrayList<IdentifyData>();
                l.add(detail);
                expandableListDetail.put(key_, l);
            }
            else {
                expandableListDetail.get(key_).add(detail);
            }
        }
        return expandableListDetail;
    }
}







public class IdentifyActivity extends Activity {



    ExpandableListView expandableIdentifyView;
    LinkedHashMap<String, List<IdentifyData>> listChild;
    List<String> listHeader;
    CustomIdentifyAdapter customIdentifyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        Intent intent = getIntent();
        getActionBar().setDisplayHomeAsUpEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_identify);
 //       SDL.loadLibrary("main");

        IdentifyData[] identify_list = (IdentifyData[]) intent.getSerializableExtra(MAPActivity.EXTRA_IDENTIFY_DATA);
        expandableIdentifyView = (ExpandableListView) findViewById(R.id.expIdentifyView);
        listChild = ExpandableIdentifyData.getData(identify_list);
        listHeader = new ArrayList<String>(listChild.keySet());



        customIdentifyAdapter = new CustomIdentifyAdapter(this, listHeader, listChild);
        expandableIdentifyView.setAdapter(customIdentifyAdapter);



    }

}





