package org.tilelessmap.app;


import android.app.Activity;
import android.os.Bundle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.widget.Toast;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;



class CustomAdapter extends BaseExpandableListAdapter {


    private Context context;
    private LinkedHashMap<String, List<Layer>> layerDetails;
    List<String> layerHeaders;


    public CustomAdapter(Context context, List<String> layerHeaders, LinkedHashMap<String, List<Layer>> layerDetails) {

        this.context = context;
        this.layerDetails = layerDetails;
        this.layerHeaders = layerHeaders;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.layerHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.layerHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);



        /*This is very hackish
        We do not maintain a separate list of layer group details
        so we rely on the layer group details of the first child
        and gives a promise to maintain the layer group details
        in all childs when changed (on the c-side)*/

        Boolean visible;

            visible = layerDetails.get(listTitle).get(0).groupVisible;


        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.layer_header, null);        }

        CheckBox cb = (CheckBox) convertView.findViewById(R.id.group_visibility);
        cb.setChecked(visible);
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout lv = (LinearLayout) v.getParent();
                TextView tv = lv.findViewById(R.id.title);
                String s = tv.getText().toString();

                CheckBox cb = (CheckBox) v;
                Layer layer = layerDetails.get(s).get(0);
                boolean isvisible = cb.isChecked();
                  if (setLayerGroupVisibility(layer.groupName,isvisible ? 1 :  0) == 0)
                      if(getChildrenCount(groupPosition) == 1)
                          setLayerVisibility(layer.id, isvisible ? 1 : 0); //If only 1 child in the group, we also have to handle it as an individual layer
                layerDetails.get(s).get(0).groupVisible = cb.isChecked();
            }
        });

        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.title);
        listTitleTextView.setText(listTitle);

        return convertView;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.layerDetails.get(this.layerHeaders.get(groupPosition)).get(childPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.layerDetails.get(this.layerHeaders.get(groupPosition)).size();
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition,final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final Layer layer = (Layer) getChild(groupPosition, childPosition);

        if (convertView == null) {

            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = layoutInflater.inflate(R.layout.layer_details, null);        }

        TextView layerTitle = (TextView) convertView.findViewById(R.id.title);
        layerTitle.setText(layer.name);

        TextView layerID = (TextView) convertView.findViewById(R.id.layerID);
        layerID.setText("Layerid: " + layer.id);

        TextView layerDbAlias = (TextView) convertView.findViewById(R.id.layerDbAlias);
        layerDbAlias.setText("DB Alias: " + layer.db_alias);

        Boolean visible = layer.visible;

        CheckBox cb_vis = (CheckBox) convertView.findViewById(R.id.layer_visibility);
        if(getChildrenCount(groupPosition) == 1) {
            cb_vis.setVisibility(View.INVISIBLE);
        }
        else {
            cb_vis.setOnCheckedChangeListener(null);

            cb_vis.setChecked(visible);
            cb_vis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                    final Layer layer = (Layer) getChild(groupPosition, childPosition);

                    CheckBox cb = (CheckBox) v;
                    boolean isvisible = cb.isChecked();
                    if (setLayerVisibility(layer.id, isvisible ? 1 : 0) == 0) {
                        layer.visible = cb.isChecked();
                    }
                }
            });
        }
            CheckBox cb_info = (CheckBox) convertView.findViewById(R.id.layer_querability);

                cb_info.setOnCheckedChangeListener(null);

                cb_info.setChecked(layer.querable);
                cb_info.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
                        final Layer layer = (Layer) getChild(groupPosition, childPosition);

                        CheckBox cb = (CheckBox) v;
                        boolean isquerable = cb.isChecked();
                        if (setLayerQuerability(layer.id, isquerable ? 1 : 0) == 0) {
                            layer.querable = cb.isChecked();
                        }
                    }
                });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    private native int setLayerVisibility(int layer_id, int set_visible);
    private native int setLayerQuerability(int layer_id, int set_visible);
    private native int setLayerGroupVisibility(String group_title_, int set_visible);
}

class ExpandableListData {
    public static LinkedHashMap<String, List<Layer>> getData(Layer[] details_list)
    {
        LinkedHashMap<String, List<Layer>> expandableListDetail = new LinkedHashMap<String, List<Layer>>();

        for (Layer detail:details_list)
        {
            if(expandableListDetail.containsKey(detail.groupName) == false) {
                List<Layer> l = new ArrayList<Layer>();
                l.add(detail);
                expandableListDetail.put(detail.groupName, l);
            }
            else {
                expandableListDetail.get(detail.groupName).add(detail);
            }
        }
        return expandableListDetail;
    }
}







public class LayerList extends Activity {




    ExpandableListView expandableListView;
    LinkedHashMap<String, List<Layer>> listChild;
    List<String> listHeader;
    CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getActionBar().setDisplayHomeAsUpEnabled(false);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layer_list);
 //       SDL.loadLibrary("main");
        Layer[] layer_list = layerlist();
        expandableListView = (ExpandableListView) findViewById(R.id.expListView);
        listChild = ExpandableListData.getData(layer_list);
        listHeader = new ArrayList<String>(listChild.keySet());



        customAdapter = new CustomAdapter(this, listHeader, listChild);
        expandableListView.setAdapter(customAdapter);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                //get the group header
                String headerInfo = listHeader.get(groupPosition);

                return false;
            }
        });


    }




    private native Layer[] layerlist();



}





