#ifndef _layerinfo_H
#define _layerinfo_H


typedef struct
{
    char    *layer_name;
    char    *layer_alias;
    char    *group_title;
    int     id;
    int     visible;
    int     group_visible;
    int     info_active;
    int     type;
    char    *db_alias;
} TLM_LAYER_INFO;

typedef struct
{
    TLM_LAYER_INFO *layers;
    int nlayers;
} TLM_LAYER_LIST;

typedef struct
{
    char *name;
    char *type;
} TLM_FIELD;
    
typedef struct
{
    TLM_LAYER_INFO layers;
    int nlayers;
} TLM_FIELDS;





/*************** Get info about layers *******************/
TLM_LAYER_LIST* TLM_get_layerlist();
int TLM_destroy_layerlist ( TLM_LAYER_LIST* tll );

extern TLM_FIELDS *TLM_get_info_fields;
extern TLM_FIELDS *TLM_get_fields;
int TLM_set_layer_visibility(int layer_id,int set_visible);
int TLM_set_layer_group_visibility(const char *group_title,int set_visible);
int TLM_set_layer_querability(int layer_id,int set_querable);

#endif
