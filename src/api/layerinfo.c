/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/

#include "layerinfo.h"
#include "../structures.h"
#include "../theclient.h"
#include "../buffer_handling.h"
#include "../ext/sqlite/sqlite3.h"
#include "../mem.h"

/*************** Get info about layers *******************/
TLM_LAYER_LIST* TLM_get_layerlist()
{
        int i;
        
        TLM_LAYER_LIST *tll = st_malloc(sizeof(TLM_LAYER_LIST));
        tll->layers = st_malloc(global_layers->nlayers * sizeof(TLM_LAYER_INFO));
        
        for(i=0;i<global_layers->nlayers;i++)
        {
            TLM_LAYER_INFO *tli = tll->layers + i;
            LAYER_RUNTIME *l = global_layers->layers + i; 
            
            tli->layer_name = st_malloc(strlen(l->name)+1);
            strcpy(tli->layer_name, l->name);
            
            tli->layer_alias = st_malloc(strlen(l->title)+1);
            strcpy(tli->layer_alias, l->title);
		
            tli->group_title = st_malloc(strlen(l->group_title)+1);
            strcpy(tli->group_title, l->group_title);
            
            tli->db_alias = st_malloc(strlen(l->db)+1);
            strcpy(tli->db_alias, l->db);
            
            tli->id = l->layer_id;
            
            tli->visible = l->visible;

            tli->group_visible = l->group_visible ;

            tli->info_active = l->info_active ;
            
            tli->info_active = l->info_active;
            
            tli->type = l->type;           
        }
        tll->nlayers = global_layers->nlayers;
        
        return tll;
}



int TLM_destroy_layerlist ( TLM_LAYER_LIST* tll )
{
        int i;
        
        for (i=0;i<tll->nlayers;i++)
        {
            TLM_LAYER_INFO *tli = tll->layers + i;
            st_free(tli->layer_name);
            st_free(tli->layer_alias);
            st_free(tli->group_title);
            st_free(tli->db_alias);            
        }
        st_free(tll->layers);
        st_free(tll);
        return 0;
}



int TLM_set_layer_visibility(int layer_id,int set_visible)
{
    int i;
    for(i=0;i<global_layers->nlayers;i++)
    {
        LAYER_RUNTIME *l = global_layers->layers + i;         
   
        if(l->layer_id == layer_id)
        {
            l->visible = set_visible;
            return 0;
        }
    }
    return 1;
}


int TLM_set_layer_group_visibility(const char *group_title,int set_visible)
{
    int i;
    for(i=0;i<global_layers->nlayers;i++)
    {
        LAYER_RUNTIME *l = global_layers->layers + i;         
   
        if(!strcmp(l->group_title, group_title))
        {
            l->group_visible = set_visible;
        }
    }
    return 0;
}


int TLM_set_layer_querability(int layer_id,int set_querable)
{
    int i;
    for(i=0;i<global_layers->nlayers;i++)
    {
        LAYER_RUNTIME *l = global_layers->layers + i;         
   
        if(l->layer_id == layer_id)
        {
            l->info_active = set_querable;
            return 0;
        }
    }
    return 1;
}

/*TODO*/
TLM_FIELDS *TLM_get_info_fields = NULL;
TLM_FIELDS *TLM_get_fields = NULL;
