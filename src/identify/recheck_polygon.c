
/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/


#include "../theclient.h"
#include "../buffer_handling.h"
#include "../info.h"
#include "../text/text.h"
#include "../utils.h"
#include "../android.h"    
#include "../tilelessmap.h"
#include "../structures.h"



int recheck_polygon(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *point, TEXT *id_list)
{
    
    int inside,n_elements;
    int64_t id;
    char id_str[32];
    int n_dims =  infoLayer->n_dims;
  POLYGON_LIST *poly = infoLayer->polygons;
  LINESTRING_LIST *wide_line = infoLayer->wide_lines;
                POLYGON_LIST *renderpoly = infoRenderLayer->polygons;
                LINESTRING_LIST *render_boundary = infoRenderLayer->wide_lines;

                unsigned int poly_n = 0;
                int next_polystart;
                if (poly->polygon_start_indexes->used>1)
                {
                    next_polystart = poly->polygon_start_indexes->list[1];
                }
                else
                {
                    next_polystart =  poly->vertex_array->used;
                    
                }
                int curr_pa_start = 0;
                int ring_n = 0;
                int next_pa_start = 0;
                int curr_poly_start = 0;
                int curr_boundary_start = 0;
                int next_boundary_start = 0;
                int save_start_boundary = 0;
                int n_elements_acc = 0;
                unsigned int pa;
                inside = 0;
                size_t stored_boundary_start_index_used; 
                size_t stored_boundary_vertex_array_used; 
                int n_vertex_in_boundary = 0;
                for (pa=0; pa<poly->pa_start_indexes->used; pa++)
                {
                    curr_pa_start = next_pa_start;
                    next_pa_start = poly->pa_start_indexes->list[pa];
                    curr_boundary_start = next_boundary_start;
                    next_boundary_start = wide_line->line_start_indexes->list[pa];



                    if(ring_n == 0 || inside)
                    {

                        if(wn_PnPoly( point,poly->vertex_array->list + curr_pa_start, (next_pa_start - curr_pa_start)/n_dims, n_dims))
                        {
                            if(ring_n == 0) //outer boundary
                            {
                                inside = 1;
                                save_start_boundary = curr_boundary_start; //Ok, we are inside a polygon, start recording the boundary
                                stored_boundary_start_index_used = render_boundary->line_start_indexes->used; //it might be that we have clicked in a hole in this polygon. So we have to also store where...to be contiues
                            }
                            else
                            {
                                /*ok, it was a hole, rewind the boundary recording*/
                                render_boundary->line_start_indexes->used = stored_boundary_start_index_used;
                                inside = 0;
                            }
                        }
                        else
                        {
                            if(ring_n == 0)                            
                                inside = 0;
                            else
                                inside = 1;
                        }
                        
                        if(inside)
                        {
                            add2gluint_list(render_boundary->line_start_indexes,render_boundary->vertex_array->used + next_boundary_start - save_start_boundary); //register start of new pa to render
                            add2pointer_list(render_boundary->style_id,system_default_info_style);
                        }
                    }


                    ring_n++;
                    if(next_polystart == next_pa_start)
                    {
                        if(inside)
                        {
                            id = infoLayer->twkb_id->list[pa];
                            log_this(100,"ok, poly for rendering");
                            infoRenderLayer->n_dims = infoLayer->n_dims;
                            add2gluint_list(renderpoly->polygon_start_indexes, renderpoly->vertex_array->used); //register start of new polygon to render
                            addbatch2glfloat_list(renderpoly->vertex_array, next_polystart - curr_poly_start, poly->vertex_array->list + curr_poly_start); //memcpy all vertexes in polygon

                            //add2gluint_list(render_boundary->line_start_indexes, render_boundary->vertex_array->used); //register start of new polygon to render
                            //addbatch2glfloat_list(render_boundary->vertex_array, next_boundary_start - curr_boundary_start, wide_line->vertex_array->list + curr_boundary_start); //memcpy all vertexes in polygon

                            size_t save_end_boundary = wide_line->line_start_indexes->list[pa];
                            addbatch2glfloat_list(render_boundary->vertex_array, save_end_boundary - save_start_boundary, wide_line->vertex_array->list + save_start_boundary); //memcpy all vertexes in boundary
                            
                            add2pointer_list(renderpoly->style_id,system_default_info_style);


                            n_elements = *(poly->element_start_indexes->list + poly_n) - n_elements_acc;

                            addbatch2glushort_list(renderpoly->element_array, n_elements, poly->element_array->list + n_elements_acc); //memcpy all vertexes in polygon
                            add2gluint_list(renderpoly->element_start_indexes, renderpoly->element_array->used); //register start of new polygon to render
                            
                            infoRenderLayer->visible = 1;
                            if(!id_list->used)
                                snprintf(id_str, 30, "%ld",id);
                            else                                
                                snprintf(id_str, 30, ", %ld",id);
                            add_txt(id_list, id_str,0);
                            
                            infoRenderLayer->type = infoLayer->type;
                        }
                        curr_poly_start = next_polystart;
                        n_elements_acc = *(poly->element_start_indexes->list + poly_n);
                        poly_n++;
                        if(poly_n < poly->polygon_start_indexes->used - 1)
                        {
                            next_polystart = poly->polygon_start_indexes->list[poly_n+1];
                        }
                        else
                        {
                            next_polystart = poly->vertex_array->used;
                        }
                      


                        ring_n = 0;
                        inside = 0;
                    }
                }
                return 0;
}



