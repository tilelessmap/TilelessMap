
/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/


#include "../theclient.h"
#include "../buffer_handling.h"
#include "../info.h"
#include "../text/text.h"
#include "../utils.h"
#include "../android.h"    
#include "../tilelessmap.h"
#include "../structures.h"
#include "identify.h"


int recheck_point(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *click_point, TEXT *id_list, float tolerance_in_meters)
{
    
    int inside;
    int64_t id;
    char id_str[32];
    int n_dims =  infoLayer->n_dims;
    POINT_LIST *point = infoLayer->points;
    POINT_LIST *renderPoint = infoRenderLayer->points;

    int next_pointstart = 0;
    int curr_point_start = 0;
    unsigned int pa;
    inside = 0;
    for (pa=0; pa<point->point_start_indexes->used; pa++)
    {
        curr_point_start = next_pointstart;
        next_pointstart = point->point_start_indexes->list[pa];
            float *p = point->points->list + curr_point_start;
            inside = check_vertex_vertex(click_point[0], click_point[1], p[0], p[1],tolerance_in_meters * 2);
            
            if(inside)
            {
                id = infoLayer->twkb_id->list[pa];
                log_this(100,"ok, point for rendering");
                infoRenderLayer->n_dims = infoLayer->n_dims;
                add2gluint_list(renderPoint->point_start_indexes, renderPoint->points->used + next_pointstart - curr_point_start); //register start of new line to render
                addbatch2glfloat_list(renderPoint->points, next_pointstart - curr_point_start, point->points->list + curr_point_start); //memcpy all vertexes in polygon

                add2pointer_list(renderPoint->style_id,system_default_info_style);

                infoRenderLayer->visible = 1;
                if(!id_list->used)
                    snprintf(id_str, 30, "%ld",id);
                else
                    snprintf(id_str, 30, ", %ld",id);
                add_txt(id_list, id_str,0);

                infoRenderLayer->type = infoLayer->type;
            }

            inside = 0;

    }
            return 0;
}
