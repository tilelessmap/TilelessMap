
#ifndef __IDENTIFY_H__
#define __IDENTIFY_H__

#include "../text/text.h"
typedef struct
{
    int layername_start;
    int id;
    int field_name_start;
    int val_start;    
}IDENTIFY_RESULT;

typedef struct
{
    int n_res;
    IDENTIFY_RESULT *identify_res;
    int n_res_alloced;    
    TEXT *data;    
}IDENTIFY_RESULTS;

extern int tolerance;
int recheck_polygon(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *point, TEXT *id_list);
int recheck_line(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *point, TEXT *id_list, float tolerance_in_meters);
int recheck_point(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *point, TEXT *id_list, float tolerance_in_meters);
int identify(MATRIX *map_matrix, int x, int y);
int destroy_identify_res(IDENTIFY_RESULTS *i_res);
int line_within_tolerance( float *P, float *V, int n, int n_dims_in_array, float tolerance);
int check_vertex_vertex(float p1x, float p1y, float p2x, float p2y,float tolerance);
#endif
