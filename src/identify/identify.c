
/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/


#include "../theclient.h"
#include "../buffer_handling.h"
#include "../info.h"
#include "../text/text.h"
#include "../utils.h"
#include "../android.h"    
#include "../tilelessmap.h"
#include "../structures.h"
#include "identify.h"
#include "../mem.h"

int tolerance = 20;

static IDENTIFY_RESULTS* init_identity_results()
{
    IDENTIFY_RESULTS *i_res = st_malloc(sizeof(IDENTIFY_RESULTS));
    i_res->n_res = 0;
    i_res->identify_res = st_malloc(sizeof(IDENTIFY_RESULT));
    i_res->n_res_alloced = 1;
    i_res->data = init_txt(128);
    
    return i_res;    
}

static int add_to_identify_res(IDENTIFY_RESULTS *i_res, char *layer_name,int id, const char *field_name, char *val)
{
    if(i_res->n_res >= i_res->n_res_alloced)
    {
        i_res->identify_res = st_realloc(i_res->identify_res, 2 * i_res->n_res_alloced * sizeof(IDENTIFY_RESULT));
        i_res->n_res_alloced *= 2;
    }
    IDENTIFY_RESULT *the_res = i_res->identify_res + i_res->n_res;
    
    the_res->layername_start = i_res->data->used;
    add_txt(i_res->data, layer_name, 1);    
    
    the_res->id = id;
    
    the_res->field_name_start = i_res->data->used;
    add_txt(i_res->data, field_name, 1);
    
    the_res->val_start = i_res->data->used;
    add_txt(i_res->data, val, 1);
    
    i_res->n_res++;
    
    return 0;
    
}

int destroy_identify_res(IDENTIFY_RESULTS *i_res)
{
    st_free(i_res->data);
    st_free(i_res->identify_res);
    st_free(i_res);
    i_res = NULL;
    return 0;
}

int reset_identify_res(IDENTIFY_RESULTS *i_res)
{
    i_res->n_res = 0;
    return 0;
}



static IDENTIFY_RESULTS* build_information_text(IDENTIFY_RESULTS *i_res, char *layer_name, TEXT *id_list)
{
 
    sqlite3_stmt *prepared_identify;
    TEXT *sql_txt = init_txt(128);
    int i;
    char num_txt[32];
    
    add_txt(sql_txt, "SELECT * FROM ", 0);
    add_txt(sql_txt, layer_name, 0);
    add_txt(sql_txt, " WHERE twkb_id in (", 0);
    add_txt(sql_txt, id_list->txt, 0);
    add_txt(sql_txt, ") ORDER BY twkb_id;", 0);
    
    
	check_sql(sql_txt->txt);

    log_this(100, "SQL in %s: %s\n",__func__, sql_txt->txt );
    int rc = sqlite3_prepare_v2(projectDB, sql_txt->txt, -1,&prepared_identify, 0);
    if (rc != SQLITE_OK ) {
        log_this(100, "SQL error in %s\n",sql_txt->txt );
        sqlite3_close(projectDB);
        return NULL;
    }
    
    int n_cols = sqlite3_column_count(prepared_identify);
    
    while (sqlite3_step(prepared_identify)==SQLITE_ROW)
    {
        int id;
        for (i=0;i<n_cols;i++)
        {            
            const char *col_name = sqlite3_column_name(prepared_identify, i);
            if(!strcmp(col_name, "twkb_id"))
                id = sqlite3_column_int(prepared_identify, i);
        }
        i=0;
        for (i=0;i<n_cols;i++)
        {
            int type = sqlite3_column_type(prepared_identify, i);
            const char *col_name = sqlite3_column_name(prepared_identify, i);
            
            
            if(type == SQLITE_INTEGER)
            {
                int val_int = sqlite3_column_int(prepared_identify, i);
                snprintf(num_txt, 32, "%d\n", val_int);
                add_to_identify_res(i_res, layer_name,id, col_name,num_txt);

            }
            else if (type == SQLITE_FLOAT)
            {
                double val_float = sqlite3_column_double(prepared_identify, i);
                snprintf(num_txt, 32, "%f\n", val_float);
                add_to_identify_res(i_res, layer_name,id, col_name,num_txt);

            }
            else if (type == SQLITE_TEXT)
            {
                const unsigned char *val_txt = sqlite3_column_text(prepared_identify, i);
                add_to_identify_res(i_res, layer_name,id, col_name,(char*) val_txt);

            }
        }

    }
    return i_res;
}



int identify(MATRIX *map_matrix, int x, int y)
{
    int tolerance_in_pixels = tolerance;
    log_this(10,"info, x=%d, y=%d\n",x,y);
    GLfloat w_x, w_y;
    int i;
    px2m(map_matrix->bbox,x,y,&w_x,&w_y);
    GLfloat meterPerPixel = (map_matrix->bbox[2]-map_matrix->bbox[0])/CURR_WIDTH;
    
    float tolerance_in_meters = tolerance_in_pixels * meterPerPixel;
//  printf("w_x = %f, w_y = %f\n", w_x, w_y);

    TEXT *id_list = init_txt(64);


    IDENTIFY_RESULTS *i_res = init_identity_results();
    
    GLfloat box[4];
    GLfloat point[2];
    point[0] = w_x;
    point[1] = w_y;
    box[0] = w_x - tolerance_in_meters * 2;
    box[1] = w_y - tolerance_in_meters * 2;
    box[2] = w_x + tolerance_in_meters * 2;
    box[3] = w_y + tolerance_in_meters * 2;



    LAYER_RUNTIME *theLayer;
    infoLayer->style_key_type = 1; //just to avoid protests when parsing data;


    infoRenderLayer->type = TYPE_ANY_WITHOUT_TEXT;
    reset_buffers(infoRenderLayer);
    infoRenderLayer->visible = 0;
    infoRenderLayer->type = 0;

    uint8_t tot_type = 0;

    for (i = 0; i<global_layers->nlayers; i++)
    {
        theLayer = global_layers->layers + i;
        //     printf("layer name %s\n",theLayer->name);
        if(theLayer->visible && theLayer->minScale<=meterPerPixel && theLayer->maxScale>meterPerPixel && theLayer->info_active)
        {
            infoLayer->type = TYPE_ANY_WITHOUT_TEXT;
            //reset all used buffers in our infoLayer
            reset_buffers(infoLayer);

            infoLayer->type = 0;


            //init more buffers if needed
            //Manipulate type, to not get unnessecary info
            if(theLayer->type & TYPE_POLYGONTYPE)
                infoLayer->type = infoLayer->type | TYPE_POLYGONTYPE  | TYPE_LINE_WIDTH; //set type to simple polygon
            else if(theLayer->type & (TYPE_LINETYPE | TYPE_LINE_WIDTH))
                infoLayer->type = infoLayer->type | TYPE_LINETYPE | TYPE_LINE_WIDTH | TYPE_INFO_TYPE;
            else if(theLayer->type & TYPE_POINTTYPE)
                infoLayer->type = TYPE_POINTTYPE | TYPE_INFO_TYPE; //set type to simple point

            // init_buffers(infoLayer);

            //Check if infoLayer holds an old prepared statemenst
            //if so unsubscribe from it
            if(infoLayer->preparedStatement->usage)
                infoLayer->preparedStatement->usage--;
            //borrow prepared statement
            infoLayer->preparedStatement = theLayer->preparedStatement;
            infoLayer->preparedStatement->usage++;
            
            //If we in the future will handle 3D, we are prepared
            infoLayer->n_dims = theLayer->n_dims;

            //get the right UTM-zone and hemisphere
            infoLayer->utm_zone = theLayer->utm_zone;
            infoLayer->hemisphere = theLayer->hemisphere;



            //add the "box, well it is just a point, but it will do
            infoLayer->BBOX = box;

            twkb_fromSQLiteBBOX(infoLayer);
            reset_txt(id_list);

            if(theLayer->type & TYPE_POLYGONTYPE)
              recheck_polygon(infoLayer,infoRenderLayer,point, id_list);
            else if (theLayer->type & (TYPE_LINETYPE | TYPE_LINE_WIDTH))
                recheck_line(infoLayer,infoRenderLayer,point, id_list, tolerance_in_meters);
            else if (theLayer->type & (TYPE_POINTTYPE))
                recheck_point(infoLayer,infoRenderLayer,point, id_list, tolerance_in_meters);

            if(id_list->used)
                i_res = build_information_text(i_res, theLayer->name, id_list);


            infoLayer->type = TYPE_ANY_WITHOUT_TEXT;
            //reset all used buffers in our infoLayer
            reset_buffers(infoLayer);
            tot_type = tot_type | infoLayer->type;
            infoLayer->type = 0;
        }



    }
    infoRenderLayer->type = tot_type;


#ifdef __ANDROID__
    return_identify(i_res);
#endif
    //setzero2pointer_list(renderpoly->style_id, renderpoly->polygon_start_indexes->used);


    //render_info(window,map_matrix->matrix);

    return 0;
}
