
/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/


#include "../theclient.h"
#include "../buffer_handling.h"
#include "../info.h"
#include "../text/text.h"
#include "../utils.h"
#include "../android.h"    
#include "../tilelessmap.h"
#include "../structures.h"
#include "identify.h"


int recheck_line(LAYER_RUNTIME *infoLayer, LAYER_RUNTIME *infoRenderLayer,GLfloat *point, TEXT *id_list, float tolerance_in_meters)
{
    
    int inside;
    int64_t id;
    char id_str[32];
    int n_dims =  infoLayer->n_dims;
    LINESTRING_LIST *line = infoLayer->lines;
    LINESTRING_LIST *wide_line = infoLayer->wide_lines;
    LINESTRING_LIST *renderline = infoRenderLayer->wide_lines;

    int next_linestart = 0;
    int curr_line_start = 0;
    int next_wide_linestart = 0;
    int curr_wide_line_start = 0;
    unsigned int pa;
    inside = 0;
    for (pa=0; pa<line->line_start_indexes->used; pa++)
    {
        curr_line_start = next_linestart;
        next_linestart = line->line_start_indexes->list[pa];
        curr_wide_line_start = next_wide_linestart;
        next_wide_linestart = wide_line->line_start_indexes->list[pa];
            inside = line_within_tolerance( point, line->vertex_array->list + curr_line_start, (next_linestart - curr_line_start)/n_dims, n_dims, tolerance_in_meters);
            if(inside)
            {
                id = infoLayer->twkb_id->list[pa];
                log_this(100,"ok, line for rendering");
                infoRenderLayer->n_dims = infoLayer->n_dims;
                add2gluint_list(renderline->line_start_indexes, renderline->vertex_array->used + next_wide_linestart - curr_wide_line_start); //register start of new line to render
                addbatch2glfloat_list(renderline->vertex_array, next_wide_linestart - curr_wide_line_start, wide_line->vertex_array->list + curr_wide_line_start); //memcpy all vertexes in polygon

                add2pointer_list(renderline->style_id,system_default_info_style);

                infoRenderLayer->visible = 1;
                if(!id_list->used)
                    snprintf(id_str, 30, "%ld",id);
                else
                    snprintf(id_str, 30, ", %ld",id);
                add_txt(id_list, id_str,0);

                infoRenderLayer->type = infoLayer->type;
            }

            inside = 0;

    }
            return 0;
}
