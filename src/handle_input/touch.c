/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/

#include "../theclient.h"
#include "../utils.h"
#include "touch.h"

FINGEREVENTS* init_touch_que()
{

    FINGEREVENTS *touches;
    touches = malloc(sizeof(FINGEREVENTS));
    touches->n_slots = MAX_ZOOM_FINGERS;
    reset_touch_que(touches);
    return touches;
}

int reset_touch_que(FINGEREVENTS *touches)
{
    int i;
    for (i=0; i<touches->n_slots; i++)
    {
        
        FINGEREVENT *fie = touches->fe + i;
        fie->active = 0;
        
    }
    touches->n_active = 0;
    return 0;
}

int register_touch_down(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y)
{
    if(touches->n_active == touches->n_slots)
        return UNKNOWN; //All slots are used, we don't support more fingers (yet)


    int i = 0;
    for (i=0; i<touches->n_slots; i++)
    {
        FINGEREVENT *fie = touches->fe + i;
        if(!fie->active)
        {
             fie->active = 1;
             fie->fingerid = fingerid;
             fie->x1 = x;
             fie->y1 = y;
             fie->x2 = x;
             fie->y2 = y;

             touches->n_active++;
            return KNOWN;
        }
    }


    return UNKNOWN;
}

void p2_to_p1(FINGEREVENTS *touches)
{    
    int i;
    FINGEREVENT *fie;
    
    for(i=0;i<touches->n_slots;i++)
    {        

        fie = touches->fe + i;
        
        fie->x1 = fie->x2;
        fie->y1 = fie->y2;        
    }    
    return;
}

void rearrange_touch_que(FINGEREVENTS *touches)
{
    int free_slot = -1;
    int i;
    FINGEREVENT *fie, *fie_free;
    
    for(i=0;i<touches->n_slots;i++)
    {
        if(!touches->fe[i].active)
            free_slot = i;
        else if(free_slot >=0)
        {
            
            fie = touches->fe + i;
            fie_free = touches->fe + free_slot;
            
            fie_free->active = 1;
            fie_free->fingerid = fie->fingerid;
            
            fie_free->x1 = fie->x1;
            fie_free->y1 = fie->y1;
            fie_free->x2 = fie->x2;
            fie_free->y2 = fie->y2;

            fie->active = 0;
            free_slot = i;
        }
    }    
}



int register_touch_up(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y)
{
    int i = 0;
    FINGEREVENT *fie;
    for (i=0; i<touches->n_slots; i++)
    {
        fie = touches->fe + i;
        if (fie->fingerid == fingerid )
        {
            if (fie->active == 1)
            {
                fie->x2 = x;
                fie->y2 = y;
                fie->active = 0;
            }
            touches->n_active--;
            return KNOWN;
        }        
    }
    
    
    return UNKNOWN;
}

int register_motion(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y)
{
    int i = 0;
    FINGEREVENT *fie;
    for (i=0; i<touches->n_slots; i++)
    {
        fie = touches->fe + i;
        if (fie->fingerid == fingerid )
        {
            fie->x2 = x;
            fie->y2 = y;
            return KNOWN;
        }
    }
    return UNKNOWN;
}



int get_box_from_touches(FINGEREVENTS *touches,MATRIX *matrix_hndl,MATRIX *ref)
{
    GLfloat w_x1,w_y1,w_x2,w_y2;
    GLfloat w_dist, px_dist;
    GLint p1x1, p1x2, p1y1, p1y2;
    GLint p2x1, p2x2, p2y1, p2y2;

    GLint deltax_px, deltay_px;
    GLfloat deltax_w, deltay_w, ratio;

    //x at first points
    p1x1 = (GLint) (touches->fe[0].x1 * CURR_WIDTH);
    p2x1 = (GLint)(touches->fe[1].x1 * CURR_WIDTH);

    //y at first points
    p1y1 = (GLint)(touches->fe[0].y1 * CURR_HEIGHT);
    p2y1 = (GLint)(touches->fe[1].y1 * CURR_HEIGHT);

    //x at second points
    if(matrix_hndl->horizontal_enabled)
    {
        p1x2 = (GLint)(touches->fe[0].x2 * CURR_WIDTH);
        p2x2 = (GLint)(touches->fe[1].x2 * CURR_WIDTH);
    }
    else
    {
        p1x2 = p1x1;
        p2x2 = p2x1;
    }

    //y at second points
    if(matrix_hndl->vertical_enabled)
    {
        p1y2 = (GLint)(touches->fe[0].y2 * CURR_HEIGHT);
        p2y2 = (GLint)(touches->fe[1].y2 * CURR_HEIGHT);
    }
    else
    {
        p1y2 = p1y1;
        p2y2 = p2y1;
    }
    px2m(ref->bbox,p1x1,p1y1,&w_x1,&w_y1);
    px2m(ref->bbox,p2x1,p2y1,&w_x2,&w_y2);

    deltax_w = w_x2 - w_x1;
    deltay_w = w_y2 - w_y1;
    w_dist = sqrtf(deltax_w * deltax_w + deltay_w * deltay_w);
    deltax_px = p2x2 - p1x2;
    deltay_px = p2y2 - p1y2;
    px_dist = sqrtf((float) (1.0 * deltax_px * deltax_px + deltay_px * deltay_px));

    if(matrix_hndl->zoom_enabled)
        ratio = w_dist/px_dist;
    else
        ratio = 1;

    matrix_hndl->bbox[0] = w_x1 - ratio * p1x2;
    matrix_hndl->bbox[1] = w_y1 - ratio * (CURR_HEIGHT - p1y2);
    matrix_hndl->bbox[2] = matrix_hndl->bbox[0] + ratio * CURR_WIDTH;
    matrix_hndl->bbox[3] = matrix_hndl->bbox[1] + ratio * CURR_HEIGHT;
    return 0;
}



