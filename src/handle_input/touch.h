
 #ifndef _touch_H
#define _touch_H

enum finger_friend {KNOWN, UNKNOWN};

typedef struct
{
    int active;
    int64_t fingerid;
    GLfloat x1;
    GLfloat y1;
    GLfloat x2;
    GLfloat y2;
}
FINGEREVENT;

typedef struct{
    FINGEREVENT fe[MAX_ZOOM_FINGERS];
    int n_slots;
    int n_active;    
}FINGEREVENTS;


FINGEREVENTS* init_touch_que();
int reset_touch_que(FINGEREVENTS *touches);
int get_box_from_touches(FINGEREVENTS *touches,MATRIX *matrix_hndl,MATRIX *out);
int register_touch_down(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y);
int register_touch_up(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y);
int register_motion(FINGEREVENTS *touches, int64_t fingerid, GLfloat x, GLfloat y);
void rearrange_touch_que(FINGEREVENTS *touches);
void p2_to_p1(FINGEREVENTS *touches);

#endif
