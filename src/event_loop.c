/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/



#include "theclient.h"
#include "handle_input/matrix_handling.h"
#include "log.h"
#include "utils.h"
#include "tilelessmap.h"
#include "handle_input/touch.h"

#ifdef __ANDROID__
#include "identify/identify.h"
#else
#include "info.h"
#endif

int map_modus = 1;
 struct CTRL *incharge = NULL;  
GLenum err = NULL;
GLuint  texes[256];


int CURR_WIDTH = 0;
int CURR_HEIGHT = 0;


#ifndef _WIN32
struct timeval tval_before, tval_after, tval_result;
#endif

int init_success = 0;

char database_name[256];
char working_dir[256];
GLfloat info_box_color[4];
sqlite3 *projectDB = NULL;


void mainLoop(SDL_Window* window,struct  CTRL *controls)
{
    log_this(100, "Entering mainLoop now\n");
    
    
    
    SDL_Event ev;
    GPSEventType = ((Uint32) -1);
    haveDBEventType = ((Uint32) -1);
    
        
     while (!projectDB && !init_success)
    {    
        
        if (SDL_WaitEvent(&ev)) /* execution suspends here while waiting on an event */
        {
            log_this(100, "ev.type %d, %d", ev.type,haveDBEventType );
            if(haveDBEventType && ev.type == haveDBEventType)
            { 
                log_this(100, "in event loop filname %s, dirname %s, %p, %p",(const char*) ev.user.data1,(const char*) ev.user.data2, ev.user.data1, ev.user.data2);
                TLM_init_db((const char*) ev.user.data1,(const char*) ev.user.data2);
                //controls = TLM_init_controls(NATIVE_default);
               log_this(30,"Yes, have db");
            }
            if(ev.type == SDL_QUIT)
                    return;                    
           
        }
    }

            
//return;
    GLfloat tx,ty;
    int mouse_down = 0;
    int wheel_y;
    SDL_Event tmp_ev[10];
    int n_events;
    GLint px_x_clicked,px_y_clicked;

    FINGEREVENTS *touches = init_touch_que();

    MATRIX map_matrix;
    MATRIX ref;
    reset_matrix(&map_matrix);
    reset_matrix(&ref);


    map_matrix.horizontal_enabled = 1;
    map_matrix.vertical_enabled = 1;
    map_matrix.zoom_enabled = 1;


    gps_point.x = 0;
    gps_point.y = 0;
    gps_point.s = 0;
    int mouse_down_x = 0, mouse_down_y = 0,mouse_up_x, mouse_up_y;

    incharge = NULL; //this means map is in charge when events occur.

//     initialBBOX(380000, 6660000, 300000, newBBOX);
//initialBBOX(230000, 6660000, 5000, newBBOX);
//  initialBBOX(325000, 6800000, 800000, newBBOX);
    //  initialBBOX(234000, 895000, 5000, newBBOX);


    //initialBBOX(init_x, init_y, init_box_width, newBBOX);
    initialBBOX(init_x, init_y, init_box_width, &map_matrix);
    initialBBOX(init_x, init_y, init_box_width, &ref); //just for saftey we init the values in ref too

//    matrixFromBBOX(newBBOX, theMatrix);
    matrixFromBBOX(&map_matrix, SET_LOCAL_ORIGO);


    while ((err = glGetError()) != GL_NO_ERROR) {
        log_this(10, "Problem 2\n");
        while ((err = glGetError()) != GL_NO_ERROR) {
            fprintf(stderr,"0 - opengl error:%d in func %s\n", err, __func__);
        }
    }

//    get_data(window, newBBOX, theMatrix);
    get_data(window, &map_matrix, controls);

    while ((err = glGetError()) != GL_NO_ERROR) {
        log_this(10, "Problem 2\n");
        fprintf(stderr,"error on return: %d\n", err);
    }
    //  copyNew2CurrentBBOX(newBBOX, currentBBOX);

    while (1)
    {
        if (SDL_WaitEvent(&ev)) /* execution suspends here while waiting on an event */
        {

            if(ev.type == GPSEventType)
            {
                render_data(window, &map_matrix, controls);
            }
            else if(ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_AC_BACK)
            {
                    free(touches);
                    return;
            }
            else
            {
                switch (ev.type) {
#ifndef __ANDROID__
                    case SDL_MOUSEBUTTONDOWN:
                        mouse_down = 1;
                        mouse_down_x = ev.button.x;
                        mouse_down_y = ev.button.y;
                        if(incharge)
                            copy2ref_box(incharge->matrix_handler,&ref);
                        else
                            copy2ref_box(&map_matrix,&ref);

                        break;
                    case SDL_MOUSEBUTTONUP:
                        mouse_down = 0;
                        mouse_up_x = ev.button.x;
                        mouse_up_y = ev.button.y;
                        if(mouse_down_x == mouse_up_x && mouse_down_y == mouse_up_y)
                        {
                            //  map_modus_before = map_modus;
                            int any_hit = check_click(controls, mouse_up_x, mouse_up_y);


                            if(! map_modus && !any_hit)
                            {
                                identify(&map_matrix, mouse_up_x,mouse_up_y);
                            }
                            render_data(window, &map_matrix, controls);

                        }
                        else
                        {
                            /*  if(map_modus)
                              {*/
                            if(!incharge)
                            {
                                matrixFromDeltaMouse(&map_matrix,&ref, mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,SET_LOCAL_ORIGO);
                                get_data(window, &map_matrix, controls);
                                //  copyNew2CurrentBBOX(newBBOX, currentBBOX);
                            }
                            else
                            {
                                matrixFromDeltaMouse(incharge->matrix_handler,&ref, mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                                render_data(window, &map_matrix, controls);
                            }
                            //}

                        }
                        break;
                    case SDL_MOUSEWHEEL:
                        wheel_y = ev.wheel.y;

                        SDL_GetMouseState(&px_x_clicked, &px_y_clicked);
                        /*  if(map_modus)
                          {*/
                        if(incharge)
                        {

                            if(wheel_y > 0)
                                matrixFromBboxPointZoom(incharge->matrix_handler,incharge->matrix_handler,px_x_clicked, px_y_clicked, 0.5,SET_LOCAL_ORIGO);
                            else
                                matrixFromBboxPointZoom(incharge->matrix_handler,incharge->matrix_handler,px_x_clicked, px_y_clicked, 2,SET_LOCAL_ORIGO);

                        }
                        else
                        {

                            if(wheel_y > 0)
                                matrixFromBboxPointZoom(&map_matrix,&map_matrix,px_x_clicked, px_y_clicked, 0.5,NO_SET_LOCAL_ORIGO);
                            else
                                matrixFromBboxPointZoom(&map_matrix,&map_matrix,px_x_clicked, px_y_clicked, 2,NO_SET_LOCAL_ORIGO);

                        }

                        get_data(window, &map_matrix, controls);

                        // copyNew2CurrentBBOX(newBBOX, currentBBOX);
                        //}
                        break;

                    case SDL_MOUSEMOTION:
                        /*  if(map_modus)
                          {*/
                        if(mouse_down)
                        {
                            n_events = 	SDL_PeepEvents(tmp_ev,3,SDL_PEEKEVENT,SDL_FIRSTEVENT,SDL_LASTEVENT);

                            if(n_events<2)
                            {
                                mouse_up_x = ev.motion.x;
                                mouse_up_y = ev.motion.y;
                                if(!incharge)
                                {
                                    matrixFromDeltaMouse(&map_matrix,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                                }
                                else
                                {
                                    matrixFromDeltaMouse(incharge->matrix_handler,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                                }
                                render_data(window, &map_matrix, controls);

                                //         copyNew2CurrentBBOX(newBBOX, currentBBOX);
                                while ((err = glGetError()) != GL_NO_ERROR) {
                                    log_this(10, "Problem 2\n");
                                    fprintf(stderr,"opengl error aaa999: %d\n", err);
                                }

                            }

                        }

                        //}
                        /*  else
                         {
                                  if(n_events<2)
                                 {

                                     mouse_up_x = ev.motion.x;
                                     mouse_up_y = ev.motion.y;

                             identify(&map_matrix, mouse_up_x,mouse_up_y,window);
                                 }
                         }*/

                        break;

#endif

                    case SDL_FINGERDOWN:
                        log_this(10, "num of active fingers = %d", touches->n_active);
                        tx = ev.tfinger.x;
                        ty = ev.tfinger.y;

                        if (register_touch_down(touches, ev.tfinger.fingerId, ev.tfinger.x,
                                                ev.tfinger.y) == UNKNOWN)
                            break;

                        if (touches->n_active == 1||1==1)
                        {
                            if (incharge)
                                copy2ref_box(incharge->matrix_handler, &ref);
                            else
                                copy2ref_box(&map_matrix, &ref);
                        }

                        p2_to_p1(touches);
                    break;


                case SDL_FINGERUP:
                    log_this(10,"SDL_FINGERUP");

                    tx = ev.tfinger.x;
                    ty = ev.tfinger.y;
                    if(register_touch_up(touches, ev.tfinger.fingerId, tx, ty) == UNKNOWN)
                        break;
                    
                    int tolerance = (int) (10 * size_factor);

                    
                    if(touches->n_active > 0)
                    {                        
                            if(!incharge)
                            {
                                get_box_from_touches(touches, &map_matrix, &ref);
                                matrixFromBBOX(&map_matrix,SET_LOCAL_ORIGO);
                                get_data(window, &map_matrix, controls);
//                                render_data(window, &map_matrix, controls);
                                copy2ref_box(&map_matrix,&ref);
                            }
                            else
                            {

                                get_box_from_touches(touches, incharge->matrix_handler,&ref);
                                matrixFromBBOX(incharge->matrix_handler, NO_SET_LOCAL_ORIGO);
                                render_data(window, &map_matrix, controls);
                                copy2ref_box(&map_matrix,&ref);
                            }


                            rearrange_touch_que(touches);
                            p2_to_p1(touches);
                            break;
                        
                    }
                    else if((fabs(touches->fe[0].x1-touches->fe[0].x2)) *  CURR_WIDTH < tolerance && (fabs(touches->fe[0].y1-touches->fe[0].y2) * CURR_HEIGHT) <tolerance)
                    {

                        int any_hit = check_click(controls, (GLint) (tx * CURR_WIDTH), (GLint)(ty * CURR_HEIGHT));

                   //     identify(&map_matrix, (GLint) (tx * CURR_WIDTH), (GLint)(ty * CURR_HEIGHT));
                        if(! map_modus && !any_hit)
                        {
                            identify(&map_matrix, (GLint) (tx * CURR_WIDTH), (GLint)(ty * CURR_HEIGHT));
                        }
                        render_data(window, &map_matrix, controls);
                        //reset_touch_que(touches);
                    }
                    else
                    {
                            //	  mouse_down = 0;
                            mouse_up_x = (GLint) (tx * CURR_WIDTH);
                            mouse_up_y = (GLint)(ty * CURR_HEIGHT);
                            mouse_down_x = (GLint)(touches->fe[0].x1 * CURR_WIDTH);
                            mouse_down_y = (GLint)(touches->fe[0].y1 * CURR_HEIGHT);
                            if(!incharge)
                            {
                                matrixFromDeltaMouse(&map_matrix,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,SET_LOCAL_ORIGO);
                                get_data(window, &map_matrix, controls);
                            }
                            else
                            {
                                matrixFromDeltaMouse(incharge->matrix_handler,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                                render_data(window, &map_matrix, controls);
                            }

                            //reset_touch_que(touches);

                        
                    }
                        reset_touch_que(touches);
                    break;

                case SDL_FINGERMOTION:
                    log_this(10,"SDL_FINGERMOTION");
                    n_events = 	SDL_PeepEvents(tmp_ev,3,SDL_PEEKEVENT,SDL_FIRSTEVENT,SDL_LASTEVENT);
                    /* if(map_modus)
                     {*/
                    
                    if(register_motion(touches, ev.tfinger.fingerId, ev.tfinger.x, ev.tfinger.y) == UNKNOWN)
                        break;
                    if(touches->n_active > 1) //check if at least 2 fingers are activated
                    {


                        if(!incharge)
                        {
                            get_box_from_touches(touches, &map_matrix, &ref);
                            matrixFromBBOX(&map_matrix,NO_SET_LOCAL_ORIGO);
                        }
                        else
                        {
                            get_box_from_touches(touches, incharge->matrix_handler, &ref);
                            matrixFromBBOX(incharge->matrix_handler,NO_SET_LOCAL_ORIGO);
                        }


                        if(n_events<2)
                            render_data(window, &map_matrix, controls);
                    }
                    else
                    {
                        tx = ev.tfinger.x;
                        ty = ev.tfinger.y;


                        //	  mouse_down = 0;
                        mouse_down_x = (GLint)(touches->fe[0].x1 * CURR_WIDTH);
                        mouse_down_y = (GLint)(touches->fe[0].y1 * CURR_HEIGHT);
                        mouse_up_x = (GLint)(touches->fe[0].x2 * CURR_WIDTH);
                        mouse_up_y = (GLint)(touches->fe[0].y2 * CURR_HEIGHT);
                        if(incharge)
                        {
                            matrixFromDeltaMouse(incharge->matrix_handler,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                        }
                        else
                        {
                            matrixFromDeltaMouse(&map_matrix,&ref,mouse_down_x,mouse_down_y,mouse_up_x,mouse_up_y,NO_SET_LOCAL_ORIGO);
                        }

                        if(n_events<2)
                            render_data(window, &map_matrix, controls);
                        //         copyNew2CurrentBBOX(newBBOX, currentBBOX);
                    }
                    //}
                    break;





                case SDL_WINDOWEVENT:
                    if (ev.window.event  == SDL_WINDOWEVENT_RESIZED)
                    {
                        windowResize(ev.window.data1,ev.window.data2,&map_matrix,&map_matrix);
//                        copyNew2CurrentBBOX(newBBOX, currentBBOX);

                        glViewport(0,0,CURR_WIDTH, CURR_HEIGHT);
                        check_screen_size();

                        if(!incharge)
                        {
                            matrixFromDeltaMouse(&map_matrix,&map_matrix,0,0,0,0,SET_LOCAL_ORIGO);
                            get_data(window, &map_matrix, controls);
                        }
                        else
                        {
                            matrixFromDeltaMouse(incharge->matrix_handler,incharge->matrix_handler,0,0,0,0,NO_SET_LOCAL_ORIGO);
                            render_data(window, &map_matrix, controls);
                        }



                    }
                    break;

                case SDL_QUIT:
                    free(touches);
                    return;
                    
                    
                }
            }
        }
        //      render_data(window,currentBBOX,theMatrix);
    }
    free(touches);
    return ;
}

