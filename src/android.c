
#include "theclient.h"
#include "api/layerinfo.h"
#include "utils.h"
#include "tilelessmap.h"
#ifdef __ANDROID__
#include <jni.h>
#include "log.h"
#include "identify/identify.h"
static JavaVM* jvm = 0;
jmethodID set_info;
jclass info_class;
jobject MapActivity_object;

JNIEnv *saved_env;

JNIEXPORT void JNICALL Java_org_tilelessmap_app_MAPActivity_onNativehaveDB(
    JNIEnv *env, jclass type, jstring the_file_,
                                                  jstring the_dir_) {
    const char *the_file = (*env)->GetStringUTFChars(env, the_file_, 0);
    const char *the_dir = (*env)->GetStringUTFChars(env, the_dir_, 0);

    register_projdb(the_file, the_dir);

    (*env)->ReleaseStringUTFChars(env, the_file_, the_file);
    (*env)->ReleaseStringUTFChars(env, the_dir_, the_dir);
}


JNIEXPORT void JNICALL Java_org_tilelessmap_app_Gps_onNativeGPS(
    JNIEnv* env, jclass jcls,
    double latitude, double longitude, double acc)
{
    gps_in(latitude, longitude, acc);
}


  


jobject Java_org_tilelessmap_app_LayerList_layerlist(
        JNIEnv *env,
        jobject jobj)
{

    int i;
    jobject tmp;
    
	TLM_LAYER_LIST *l_list = TLM_get_layerlist();
    
    
    int numResultElements = l_list->nlayers;


    jclass resultClass = (*env)->FindClass(env, "org/tilelessmap/app/Layer");
    jmethodID resultClassConstructorMID = (*env)->GetMethodID(env, resultClass, "<init>", "()V");


    //instantiate the result object element array
    jobjectArray resultElementsArray = (*env)->NewObjectArray(env, numResultElements, resultClass, NULL);



/*the signatures described here:	https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/types.html#wp276*/
    jfieldID resultElementName = (*env)->GetFieldID(env, resultClass, "name", "Ljava/lang/String;");
    jfieldID resultElementVisible = (*env)->GetFieldID(env, resultClass, "visible", "Z");
    jfieldID resultElementGroupName = (*env)->GetFieldID(env, resultClass, "groupName", "Ljava/lang/String;");
    jfieldID resultElementGroupVisible = (*env)->GetFieldID(env, resultClass, "groupVisible", "Z");
    jfieldID resultElementQuerable = (*env)->GetFieldID(env, resultClass, "querable", "Z");
    jfieldID resultElementId = (*env)->GetFieldID(env, resultClass, "id", "I");
    jfieldID resultElementdb_alias = (*env)->GetFieldID(env, resultClass, "db_alias", "Ljava/lang/String;");



    for (i=0;i<numResultElements;i++)
    {
        TLM_LAYER_INFO *l = l_list->layers + i;
        tmp = (*env)->NewObject(env, resultClass, resultClassConstructorMID, 1);

        (*env)->SetObjectField(env, tmp, resultElementName, (*env)->NewStringUTF(env, l->layer_alias));
        (*env)->SetObjectField(env, tmp, resultElementGroupName, (*env)->NewStringUTF(env, l->group_title));
        (*env)->SetBooleanField(env, tmp, resultElementVisible, l->visible);
        (*env)->SetBooleanField(env, tmp, resultElementGroupVisible, l->group_visible);
        (*env)->SetBooleanField(env, tmp, resultElementQuerable, l->info_active);
        (*env)->SetIntField(env, tmp, resultElementId, l->id);
        (*env)->SetObjectField(env, tmp, resultElementdb_alias, (*env)->NewStringUTF(env, l->db_alias));

        (*env)->SetObjectArrayElement(env, resultElementsArray, i, tmp);
        (*env)->DeleteLocalRef(env, tmp);

    }
    TLM_destroy_layerlist(l_list);
    return resultElementsArray;
}

  

JNIEXPORT int JNICALL Java_org_tilelessmap_app_CustomAdapter_setLayerVisibility(
    JNIEnv* env, jclass jcls,
    int layer_id, int set_visible)
{
    return TLM_set_layer_visibility(layer_id, set_visible);
}

JNIEXPORT int JNICALL Java_org_tilelessmap_app_CustomAdapter_setLayerGroupVisibility(
    JNIEnv* env, jclass jcls,
    jstring group_title_, int set_visible)
{

    const char *group_title = (*env)->GetStringUTFChars(env, group_title_, 0);
    return TLM_set_layer_group_visibility(group_title, set_visible);
}

JNIEXPORT int JNICALL Java_org_tilelessmap_app_CustomAdapter_setLayerQuerability(
    JNIEnv* env, jclass jcls,
    int layer_id, int set_querable)
{
    return TLM_set_layer_querability(layer_id, set_querable);
}

JNIEXPORT int JNICALL Java_org_tilelessmap_app_MAPActivity_InfoMode(
    JNIEnv* env,jclass jcls,
    int start, jobject ma )
{
    (*env)->GetJavaVM(env,&jvm); //We store the jvm reference to use when calling back after a click
    
    saved_env = env;
    MapActivity_object = (*env)->NewGlobalRef(env, ma);
    // Find the Java class - provide package ('.' replaced to '/') and class name
       jclass resultClass =  (*env)->FindClass(env, "org/tilelessmap/app/Layer");

        map_modus = !start;
        infoRenderLayer->visible = start;
        
        if(!start)
            (*env)->DeleteGlobalRef(env, MapActivity_object);
    return 0;
}


JNIEXPORT int JNICALL Java_org_tilelessmap_app_SettingsActivity_IdentifyTolerance(
    JNIEnv* env,jclass jcls,
    int tol, jobject ma )
{
    tolerance = tol;
    return 0;
}





void return_identify(IDENTIFY_RESULTS *i_res)
{
    
    int i;

    jobject tmp;

JNIEnv *env;
//    JNIEnv *env = saved_env;
    jint rs = (*jvm)->AttachCurrentThread(jvm, &env, NULL);//create JNIEnv from JavaVM





//    info_class = (*env)->FindClass(env, (*env)->GetObjectClass(env, MapActivity_object));
    info_class = (*env)->FindClass(env,"org/tilelessmap/app/MAPActivity");

//    jmethodID identifyClassConstructorMID = (*env)->GetMethodID(env, info_class, "<init>", "()V");
//    (*env)->NewObject(env, info_class, identifyClassConstructorMID, 1);

    set_info = (*env)->GetMethodID(env, info_class, "identify_info", "([Lorg/tilelessmap/app/IdentifyData;)V");

    //env->CallVoidMethod(cbObject, succ_method, abcd);




    jclass resultClass = (*env)->FindClass(env, "org/tilelessmap/app/IdentifyData");    

    jmethodID resultClassConstructorMID = (*env)->GetMethodID(env, resultClass, "<init>", "()V");


    //instantiate the result object element array
    jobjectArray resultElementsArray = (*env)->NewObjectArray(env, i_res->n_res, resultClass, NULL);



/*the signatures described here:	https://docs.oracle.com/javase/7/docs/technotes/guides/jni/spec/types.html#wp276*/
    jfieldID resultElementLayerName = (*env)->GetFieldID(env, resultClass, "layerName", "Ljava/lang/String;");
    jfieldID resultElementId = (*env)->GetFieldID(env, resultClass, "id", "I");
    jfieldID resultElementColName = (*env)->GetFieldID(env, resultClass, "colName", "Ljava/lang/String;");
    jfieldID resultElementVal = (*env)->GetFieldID(env, resultClass, "val", "Ljava/lang/String;");



    for (i=0;i < i_res->n_res;i++)
    {
        IDENTIFY_RESULT *the_res = i_res->identify_res + i;
        
        char *the_str;
        
        tmp = (*env)->NewObject(env, resultClass, resultClassConstructorMID, 1);

        the_str = i_res->data->txt + the_res->layername_start;
        log_this(100, "layername: %s\n", the_str);
        (*env)->SetObjectField(env, tmp, resultElementLayerName, (*env)->NewStringUTF(env, i_res->data->txt + the_res->layername_start));
        (*env)->SetIntField(env, tmp, resultElementId, the_res->id);
        (*env)->SetObjectField(env, tmp, resultElementColName, (*env)->NewStringUTF(env, i_res->data->txt + the_res->field_name_start));
        (*env)->SetObjectField(env, tmp, resultElementVal, (*env)->NewStringUTF(env, i_res->data->txt + the_res->val_start));

        (*env)->SetObjectArrayElement(env, resultElementsArray, i, tmp);
        (*env)->DeleteLocalRef(env, tmp);

    }
    
    (*env)->CallVoidMethod(env, MapActivity_object, set_info, resultElementsArray);
    
    destroy_identify_res(i_res);
 //   (*jvm)->DetachCurrentThread(jvm);
    return;
}



#endif
