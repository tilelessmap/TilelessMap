
/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/

#include "theclient.h"

int check_vertex_vertex(float p1x, float p1y, float p2x, float p2y,float tolerance)
{
    float deltax = p1x - p2x;
    float deltay = p1y - p2y;
    
    if(deltax * deltax + deltay * deltay <= tolerance * tolerance)
        return 1;
    else
        return 0;
}





int line_within_tolerance( float *P, float *V, int n, int n_dims_in_array, float tolerance)
{
    int res = 0;
    int i;
    float p2x, p2y, ppx, ppy;
    float p1x = V[0]; //first x
    float p1y = V[1];                        //first y
    
    for (i=1; i<n; i++)
    {
        /*Check if first point in segment is closer to point than tolerance*/
        if(check_vertex_vertex(p1x, p1y, P[0], P[1],tolerance))
            return 1;

        p2x = p1x;
        p2y=p1y;
        
        p1x = V[i * n_dims_in_array];
        p1y = V[i * n_dims_in_array + 1];

        
        double	r = ( (P[0]-p1x) * (p2x-p1x) + (P[1]-p1y) * (p2y-p1y) )/( (p2x-p1x)*(p2x-p1x) +(p2y-p1y)*(p2y-p1y ));

        /*If point projected on segment line is between p1 and p2*/
        if(r > 0 && r < 1)
        {
            /*If point is on segment*/
            if ( (p1y-P[1])*(p2x-p1x==(p1x-P[0])*(p2y-p1y) ) )
                return 1;
            
            /*calculate projected point on segment and check distance from that point to original point*/
            ppx = p1x + r * (p2x-p1x);
            ppy=p1y + r * (p2y-p1y);
            if(check_vertex_vertex(ppx, ppy, P[0], P[1],tolerance))
                return 1;                   
            
        }
    }
    
    /*Last point is not checked yet, let's do that*/
    if(check_vertex_vertex(p2x, p2y, P[0], P[1],tolerance))
    return 1;
 
    /*Ok, it wasn't close enough*/
    return 0;
}
//===================================================================
