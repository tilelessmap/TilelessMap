/**********************************************************************
 *
 * TilelessMap
 *
 * TilelessMap is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * TilelessMap is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TilelessMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************
 *
 * Copyright (C) 2016-2018 Nicklas Avén
 *
 ***********************************************************************/




#ifndef _theclient_H
#define _theclient_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <time.h>
#ifndef _WIN32
#include <sys/time.h>
#endif
#include<pthread.h>
#include "ext/sqlite/sqlite3.h"
//#include <sqlite3.h>

/* Use glew.h instead of gl.h to get all the GL prototypes declared */
#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <GL/glew.h>
#endif
/* Using SDL2 for the base window and OpenGL context init */
#include "SDL.h"
#include "text/text.h"
#include "global.h"
#include "handle_input/matrix_handling.h"
#include "symbols.h"
#include "structures.h"
#define INIT_WIDTH 1000
#define INIT_HEIGHT 500

//Set this to 0 to do the data fetching in serial. Good for debugging
#define THREADING 1

#define DEFAULT_TEXT_BUF 1024

#define MAX_ZOOM_FINGERS 2


#define INIT_PS_POOL_SIZE 10


/*twkb types*/
#define	POINTTYPE			1
#define	LINETYPE			2
#define	POLYGONTYPE		3
#define	MULTIPOINTTYPE	4
#define	MULTILINETYPE		5
#define	MULTIPOLYGONTYPE	6
#define	COLLECTIONTYPE		7
#define	RASTER		        128


int init_resources(const char *dir);



/*************Memory handling***********/


/*void destroy_buffer(GLESSTRUCT *res_buf);
int check_and_increase_max_pa(size_t needed, GLESSTRUCT *res_buf);
float* get_start(uint32_t npoints, uint8_t ndims, GLESSTRUCT *res_buf);
int set_end(uint32_t npoints, uint8_t ndims,uint32_t id, uint32_t styleID, GLESSTRUCT *res_buf);

int check_and_increase_max_polygon(uint32_t needed, GLESSTRUCT *res_buf);
int set_end_polygon( GLESSTRUCT *res_buf);


ELEMENTSTRUCT* init_element_buf();
void element_destroy_buffer(ELEMENTSTRUCT *element_buf);
int element_check_and_increase_max_pa(size_t needed, ELEMENTSTRUCT *element_buf);
GLushort* element_get_start(uint32_t npoints, uint8_t ndims, ELEMENTSTRUCT *element_buf);
int element_set_end(uint32_t npoints, uint8_t ndims,uint32_t styleID, ELEMENTSTRUCT *element_buf);
GLfloat* increase_buffer(GLESSTRUCT *res_buf);
*/
/*Functions exposed to other programs*/
void *twkb_fromSQLiteBBOX_thread( void *theL);
void *twkb_fromSQLiteBBOX( void *theL);
GLuint create_shader(const char* source, GLenum type);
void print_log(GLuint object);



//void initialBBOX(GLfloat x, GLfloat y, GLfloat width, GLfloat *newBBOX);

/*event handling*/
//int matrixFromBboxPointZoom(MATRIX *map_matrix,MATRIX *out,GLint px_x_clicked,GLint px_y_clicked, GLfloat zoom);
int matrixFromBboxPointZoom(MATRIX *matrix_hndl,MATRIX *ref, GLint px_x_clicked, GLint px_y_clicked, GLfloat zoom, uint8_t set_local_origo);
//int matrixFromDeltaMouse(GLfloat *currentBBOX,GLfloat *newBBOX,GLfloat mouse_down_x,GLfloat mouse_down_y,GLfloat mouse_up_x,GLfloat mouse_up_y, GLfloat *theMatrix);
int matrixFromDeltaMouse(MATRIX *map_matrix,MATRIX *out, GLint mouse_down_x, GLint mouse_down_y, GLint mouse_up_x, GLint mouse_up_y, uint8_t set_local_origo);
LAYER_RUNTIME* init_layer_runtime(int n);
LAYERS* init_layers(int n);
int  matrixFromBBOX(MATRIX *map_matrix , uint8_t set_local_origo);
//int get_data(SDL_Window* window,GLfloat *bbox,GLfloat *theMatrix);
int get_data(SDL_Window* window,MATRIX *map_matrix,struct CTRL *controls);

int loadPoint(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int  renderPoint(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int loadLine(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int  renderLine(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int renderLineTri(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);

void render_txt(SDL_Window* window) ;
int loadPolygon(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int  renderPolygon(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int render_data(SDL_Window* window,MATRIX *theMatrix, struct CTRL *controls);
int render_info(SDL_Window* window,GLfloat *theMatrix);


int loadRaster(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int renderRaster(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
int loadandRenderRaster(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);


GLuint create_program(const unsigned char *vs_source,const unsigned char *fs_source, GLuint *vs, GLuint *fs);

void reset_shaders(GLuint vs,GLuint fs,GLuint program);

//uint32_t utf82unicode(char *text, char **the_rest);
int init_text_resources();


void log_this(int log_level, const char *log_txt, ... );
//int draw_it(GLfloat *color,GLfloat *point_coord, int atlas_nr,int bold,GLint txt_box,GLint txt_color,GLint txt_coord2d,char *txt,GLint max_width, float sx, float sy);
//int draw_it(GLfloat *color,GLfloat *startp,GLfloat *offset, int atlas_nr,int bold,GLint txt_box,GLint txt_color,GLint txt_coord2d,char *txt,GLint max_width, float sx, float sy);
int draw_it(GLfloat *color,GLfloat *startp,GLfloat *offset,ATLAS *a/* int atlas_nr,int bold*/,GLint txt_box,GLint txt_color,GLint txt_coord2d,char *txt,GLint max_width, float sx, float sy);
//int print_txt(float x,float y,float r, float g, float b, float a,int size, const char *txt, ... );
//int print_txt(GLfloat *point_coord,GLfloat *color,int size,int bold,int max_width, const char *txt, ... );
//int render_simple_rect(GLfloat minx, GLfloat miny, GLfloat maxx, GLfloat maxy);
int render_simple_rect(GLshort *coords, GLfloat *color, MATRIX *matrix_hndl);

void calc_start(POINT_CIRCLE *p,GLFLOAT_LIST *ut,int *c, t_vec2 *last_normal);
void calc_join(POINT_CIRCLE *p,GLFLOAT_LIST *ut,int *c, t_vec2 *last_normal);
void calc_end(POINT_CIRCLE *p,GLFLOAT_LIST *ut,int *c, t_vec2 *last_normal);

int build_program();
int check_layer(const unsigned char *dbname, const unsigned char  *layername);


void gps_in(double latitude, double longitude, double acc);
int renderGPS(MATRIX *matrix_hndl);
int loadGPS(GLfloat *gps_circle);
int loadSymbols();
void reproject ( double* points, uint8_t utm_in, uint8_t utm_out, uint8_t hemi_in, uint8_t hemi_out );
int check_column(const unsigned char *dbname,const unsigned char * layername, const unsigned char  *col_name);


/*********************** Global variables*******************************/


#ifndef _WIN32
extern struct timeval tval_before, tval_after, tval_result;
#endif

extern int init_success;

extern char database_name[256];
extern char working_dir[256];
extern int map_modus;
extern GLfloat info_box_color[4];
extern sqlite3 *projectDB;

extern GLfloat init_x;
extern GLfloat init_y;
extern GLint curr_utm;
extern GLint curr_hemi;
extern GLfloat init_box_width;
//int nLayers;
extern int text_scale;
extern GLfloat *gps_circle;

extern LAYERS *global_layers;
//LAYER_RUNTIME *layerRuntime;
extern LAYER_RUNTIME *infoLayer;
extern LAYER_RUNTIME *infoRenderLayer;

/*
STYLES_RUNTIME *global_styles;
size_t length_global_styles;
size_t length_global_symbols;*/
void render_text_test(const char *text, float x, float y, float sx, float sy);
int load_text(LAYER_RUNTIME *oneLayer);
int  render_text(LAYER_RUNTIME *oneLayer,GLfloat *theMatrix);
extern int CURR_WIDTH;
extern int CURR_HEIGHT;



//extern const char *fontfilename;
/*shader programs*/

//Standard geometryprogram
extern GLuint std_program;
extern GLint std_coord2d;
extern GLint std_matrix;
extern GLint std_color;

//Standard textprogram
extern GLuint txt_program;
extern GLint txt_coord2d;
extern GLint txt_matrix;
extern GLint txt_box;
extern GLint txt_tex;
extern GLint txt_texpos;
extern GLint txt_color;

//new textprogram
extern GLuint txt2_program;
extern GLint txt2_coord2d;
extern GLint txt2_matrix;
extern GLint txt2_px_matrix;
extern GLint txt2_delta;
extern GLint txt2_box;
extern GLint txt2_tex;
extern GLint txt2_texpos;
extern GLint txt2_color;

extern GLuint gen_vbo;


//Standard geometryprogram
extern GLuint lw_program;
extern GLint lw_coord2d;
extern GLint lw_matrix;
extern GLint lw_px_matrix;
extern GLint lw_linewidth;
extern GLint lw_norm;
extern GLint lw_color;
extern GLint lw_z;

//gps-void calc_end(POINT_CIRCLE* p, GLfloat* ut, int* c, vec2* last_normal)

extern GLuint gps_program;
extern GLint gps_norm;
extern GLint gps_coord2d;
extern GLint gps_radius;
extern GLint gps_color;
extern GLint gps_matrix;
extern GLint gps_px_matrix;

extern GLuint sym_program;
extern GLint sym_norm;
extern GLint sym_coord2d;
extern GLint sym_radius;
extern GLint sym_color;
extern GLint sym_matrix;
extern GLint sym_px_matrix;
extern GLint sym_z;

extern GLuint raster_program;
extern GLint raster_coord2d;
extern GLint raster_texcoord;
extern GLint raster_matrix;
extern GLint raster_texture;


typedef struct  {
    GLfloat x;
    GLfloat y;
    GLfloat s;
    GLfloat t;
} point;

extern point gps_point;
extern Uint32 GPSEventType;
extern Uint32 haveDBEventType;
extern GLuint text_vbo;
extern int gps_npoints;
extern GLuint gps_vbo;

extern int total_points;
extern int n_points;
extern int n_lines;
extern int n_polys;
extern int n_tri;
extern int n_words;
extern int n_letters;
extern int mod_points;

extern GLuint  texes[256];
extern GLenum err;
#endif
